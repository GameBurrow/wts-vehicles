# WTS-Vehicles

![https://i.imgur.com/eQHOnKk.jpg](https://i.imgur.com/eQHOnKk.jpg)

Front destination displays for Write The Signs (Cities Skylines) mod for varius Steam Workshop vehicles. I've mostly only added front (now started adding back ones also) destination displays for Buses, Trolleybuses and Trams (that I currently have in use). All of the vehicles in use can be found in my Vehicles - Transport workshop collection: https://steamcommunity.com/sharedfiles/filedetails/?id=941827177
## Mod page
- Github: https://github.com/klyte45/WriteTheSigns
- Steam Workshop: https://steamcommunity.com/sharedfiles/filedetails/?id=2139980554&searchtext=write+the+sign

# Installing these
Copy the contents of this pack into: `%LOCALAPPDATA%\Colossal Order\Cities_Skylines\Klyte45Mods\WriteTheSigns\VehiclesDefaultPlacing`